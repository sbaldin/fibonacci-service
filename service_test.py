import pytest
from service import init_app, fibonacci

@pytest.fixture
async def client(aiohttp_client):
    app = init_app()
    return await aiohttp_client(app)

async def test_fibonacci():
    res = await fibonacci(10)
    assert res == 34

    with pytest.raises(TypeError):
        await fibonacci('10')
    
    with pytest.raises(ValueError):
        await fibonacci(0)


async def test_get_fibonacci(client):
    resp = await client.get('/fibonacci?from=5&to=10')
    assert resp.status == 200

    resp = await client.get('/fibonacci')
    assert resp.status == 200


async def test_wrong_params(client):
    resp = await client.get('/fibonacci?from=somestring')
    assert resp.status == 400

    resp = await client.get('/fibonacci?from=10&to=5')
    assert resp.status == 400
