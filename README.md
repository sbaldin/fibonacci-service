# Fibonacci Web Service
Simple web service that retrieves fibonacci sequense

To start the service open terminal and run commands

    docker-compose up -d

[Docker](https://www.docker.com/) and [docker-compose](https://docs.docker.com/compose/) must be installed on your system

Service will start on port `:8000`

To run tests after container is up execute the following command

    docker-compose run app pytest


To obtain fibonacci sequense hit endpoint

    GET /fibonacci

To get a slice you may pass optional parameters in a query string

    GET /fibonacci?from=10&to=1000

