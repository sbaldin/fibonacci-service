import asyncio
import sys
import os
import logging
import aiohttp.web as web

from typing import List
from aiocache import cached, caches

logging.basicConfig(level=logging.ERROR)
logger = logging.getLogger(__name__)


MIN_SLICE_VALUE = int(os.environ.get('MIN_SLICE_VALUE', 1))
MAX_SLICE_VALUE = int(os.environ.get('MAX_SLICE_VALUE', 10))

caches.set_config({
        'default': {
            'cache': "aiocache.SimpleMemoryCache",
        },
        'redis': {
            'cache': "aiocache.RedisCache",
            'endpoint': "redis",
            'port': 6379,
            'serializer': {
                'class': 'aiocache.serializers.StringSerializer'
            },
        }
    })

use_chache = cached(alias='redis')


@use_chache
async def fibonacci(n: int) -> int:
    if type(n) != int:
        raise TypeError("argument must be a positive integer")
    if n < 1:
        raise ValueError("argument must be a positive integer")
    if n == 1:
        return 0
    if n == 2:
        return 1
    else:
        x = await fibonacci(n-1)
        y = await fibonacci(n-2)
        return int(x) + int(y)


async def handler(request: web.Request) -> web.Response:
    start, end = request.query.get('from'), request.query.get('to')
    if start is None:
        start = MIN_SLICE_VALUE
    if end is None:
        end = MAX_SLICE_VALUE

    try:
        start, end = int(start), int(end)
        if start > end:
            raise ValueError
        result = [await fibonacci(n) for n in range(start, end+1)]
        
    except (TypeError, ValueError) as e:
        raise web.HTTPBadRequest(text='Invalid request parameters')
    except Exception as e:
        logger.exception(str(e))
        raise web.HTTPInternalServerError
    return web.json_response(result)


def init_app() -> web.Application:

    app = web.Application()

    app.router.add_get(r'/fibonacci', handler)

    return app


def main(argv: List[str]) -> None:
    try:
        app = init_app()
        web.run_app(app, host='0.0.0.0', port=8000)
    except Exception as e:
        logger.exception(str(e))


if __name__ == "__main__":
    main(sys.argv[1:])
